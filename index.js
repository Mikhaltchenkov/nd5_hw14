
console.log("----- Часть № 1 -----");

const fs = require("fs");
const crypto = require('crypto');

const input = fs.createReadStream("data.txt");
const output = fs.createWriteStream("hash_bin.txt");
const hash = crypto.createHash('sha256');

input.pipe(hash).pipe(output);

console.log("----- Часть № 2 -----");

const stream = require("stream");
const output_text = fs.createWriteStream("hash_text.txt");

var ts = new stream.Transform();

ts._transform = function (chunk, encoding, done) {
	let chunk_enc = chunk.toString('hex');
	this.push(chunk_enc);
	console.log(chunk_enc);
	done();
}

input.pipe(hash).pipe(ts).pipe(output_text);